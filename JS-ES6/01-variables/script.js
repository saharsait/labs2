//console.log(document);

/*
var name = "Sahar";
console.log(name);
name = "Mohammad";
console.log(name);

let city = 'riyadh';
console.log(city);

const city2 = 'Dammam';
console.log(city2);

const str = 'Hi Am Avatar';
console.log(str);
console.log(typeof str);

const bL = true;
console.log(typeof bL);

let n = null;
console.log(typeof n); 
const kid = 'Bobo';
const gender = 'Female';
if (gender === 'Male') {

    console.log(kid + ' is my son');
    
} else {
    console.log(kid + ' is my daughter');
}

const prof = 'dev';
if (prof === 'composer') {
    console.log(prof + ' is a music creater');
} else if (prof === 'dev') {
    console.log(prof + 'is a developer');
}

const firstName = 'Sahar';
const secondName = 'Hani';
const lastName = 'Sait';
//console.log(firstName + ' ' + secondName + ' ' + lastName); 
console.log(`${firstName} ${secondName}`); //this method prints strings with spaces in between 
*/


/*
let nameIsChecked = true;
let ageIsChecked = false;

let isGreater = 4 > 1;
console.log(isGreater);
*/


/*
let id = Symbol(); //Symbol: generates a unique identifier for each data 
Symbol('Sahar') === Symbol('Sahar');

let a = 5;
let b = 10;

let c = a;

a = b;
b = c;

console.log(a);
console.log(b);

*/

//conditional statements
//=== equality
//!== not equal
/*
let age = 20;
switch(age){
    case 10: console.log("this is 10"); 
        break;
    case 12: console.log("this is 12");
        break;
    default: console.log("hehe");
        break;
}
*/
/*
let age = 20;
let result = age > 20 ? true : false;
console.log(result);*/
/*
let person = {
    name: 'sahar',
    age: 23,
    salary:5
}

console.log(person.age);
console.log(person['name']);
delete person.salary;
console.log(person.salary);

//array
let age = [12, 13, 40, 50];
console.log(age[0]);
console.log(age.length);
console.log(age[0] = 1);
console.log(age[0]);
console.log(age);
age.pop();
console.log(age);

console.log(age[3]);
//age.shift();
age.unshift(60);
console.log(age);

age.push(80);
console.log(age);
age.splice(3, 0, 2);//3: index, 0:add a value, 2: the value to be added
age.splice(3, 1, 2);//3: index, 1:delete one value, 2: the value to be added
console.log(age);

age.forEach((elements) => {
    console.log(elements);
});

age.indexOf(50);

*/
/*
let names = ['Sahar', 'Mohammad', 'Suzana'];
console.log(names.join('-'));*/
/*
let arr1 = [1, 2, 3, 4];
let arr2 = [...arr1, 5];//the "...arr1" copies all the elements of arr1 into arr2
console.log(arr2);*/


/* merging objects

const obj1 = {
    greetings: 'Hello',
    name: 'Sahar',
    age: 23
}

const obj2 = {
    ...obj1, //this will copy data from prev obj
    greetings: 'Hi', //this will update the previous greetings
    newVal: 17
}
console.log(obj2);

*/


/* while loop
let i = 1, n = 5;
let arr = [];
while (i <= n) {
    console.log(`the number is ${i}`);   
    if (i === 3 || i == 4) {
        arr.push(i);
    }
    i++;
}
console.log(arr);

*/


/* for loop
// var is global, let is scope based. var can be used anywhere, let respects the scope

for (var row = 0; row <= 7; row++){
    console.log('any');
}
console.log(row);

*/

// using for in
/*
const student = {
    name: 'Sahar',
    class: 1,
    age: 3
}

for (let key in  student){
    console.log(`${key} ====> ${student[key]}`);
}
*/

// using for of
/*
const students = ['A', 'B', 'C'];
for (let elm of students){
    console.log(elm);
}

// in is for index and of is for content
*/


//function expression: storing a funciton inside of a vairable
/*
const connectionDb = function () {

}
*/

/*

function passExam(name, score) {
    const passUniversity = 71;
    const passCollege = 51;
    if (score >= passUniversity) {
        console.log(name + " Enrolled in university with " + score + " points");
    } else if (score >= passCollege) {
        console.log(name + " Enrolled in college with " + score + " points");
    } else {
        console.log(name + "you faield lol");
    }
}

function totalScore(exam, labs) {
    return exam + labs;
}

passExam("sahar", totalScore(13, 67));

*/

let add = function (x, y) {
    return x + y;
}

console.log(add(1,2));

const multi = (x, y) => x * y;

console.log(multi(5,4));